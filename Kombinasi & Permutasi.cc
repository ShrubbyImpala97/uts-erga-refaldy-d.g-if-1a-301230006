#include <iostream>

using namespace std;

int fact(int n) {
    if (n == 0 || n == 1)
    return 1;
    else
    return n * fact(n - 1);
}
int main() {
    int n, r, comb, per;
    cout<<"Masukkan n : ";
    cin>>n;
    cout<<"\nMasukkan r : ";
    cin>>r;
    comb = fact(n) / (fact(r) * fact(n-r));
    cout <<"\nKombinasi : " << comb;
    per = fact(n) / fact (n-r);
    cout << "\nPermutasi : " << per;
    return 0;
}
