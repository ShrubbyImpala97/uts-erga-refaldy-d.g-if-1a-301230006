#include <iostream>
using namespace std;

int main() {
    float bilangan1, bilangan2, hasil;
    char operasi;

    cout << "Masukan Bilangan ke 1      : ";
    cin >> bilangan1;

    cout << "Masukan Operator (x,:,+,-) : ";
    cin >> operasi;

    cout << "Masukan Bilangan ke 2      : ";
    cin >> bilangan2;

    switch (operasi) {
        case '+':
            hasil = bilangan1 + bilangan2;
            cout << "Bilangan ke 1 + Bilangan ke 2 = " << bilangan1 << " + " << bilangan2 << " = " << hasil;
            break;
        case '-':
            hasil = bilangan1 - bilangan2;
            cout << "Bilangan ke 1 - Bilangan ke 2 = " << bilangan1 << " - " << bilangan2 << " = " << hasil;
            break;
        case 'x':
            hasil = bilangan1 * bilangan2;
            cout << "Bilangan ke 1 x Bilangan ke 2 = " << bilangan1 << " x " << bilangan2 << " = " << hasil;
            break;
        case ':':
            if (bilangan2 != 0) {
                hasil = bilangan1 / bilangan2;
                cout << "Bilangan ke 1 : Bilangan ke 2 = " << bilangan1 << " : " << bilangan2 << " = " << hasil;
            } else {
                cout << "Error! Pembagian dengan nol tidak dapat dilakukan.";
            }
            break;
        default:
            cout << "Operator yang dimasukkan tidak valid!";
            break;
    }

    return 0;
}
