#include <iostream>
#include <stdlib.h>

using namespace std;

void jdl_aplikasi() {
    cout << "*************************" << endl;
    cout << "*  Aplikasi Hitung Gaji *" << endl;
    cout << "*************************" << endl << endl;
}

void input(string &nama, char &gol, string &status) {
    cout << "Nama Karyawan       : ";
    cin >> nama;
    cout << "Golongan (A\\B)      : ";
    cin >> gol;
    cout << "Satus (Nikah\\Belum) : ";
    cin >> status;
}

void hitungTunjanganGapok(char gol, string status, float &gapok, float &tunja) {
    switch (gol) {
        case 'A':
            gapok = 200000;
            if (status == "Nikah") {
                tunja = 50000;
            } else {
                if (status == "Belum") {
                    tunja = 25000;
                }
            }
            break;
        case 'B':
            gapok = 350000;
            if (status == "Nikah") {
                tunja = 75000;
            } else {
                if (status == "Belum") {
                    tunja = 60000;
                }
            }
            break;
        default:
            cout << " Salah Input Golongan !!!" << endl;
            break;
    }
}

float hitungPotongan(float gapok, float tunja) {
    float prosen_pot = 0.05;
    if (gapok > 300000) {
        prosen_pot = 0.1;
    }
    return (gapok + tunja) * prosen_pot;
}

float hitungGajiBersih(float gapok, float tunja, float pot) {
    return (gapok + tunja) - pot;
}

void output(float gapok, float tunja, float pot, float gaber) {
    cout << "Gaji Pokok     : Rp." << gapok << endl;
    cout << "Tunjangan      : Rp." << tunja << endl;
    cout << "Potongan Iuran : Rp." << pot << endl;
    cout << "Gaji Bersih    : Rp." << gaber << endl;
}

int main() {
    system("clear");

    string nama = " ";
    char gol = ' ';
    string status = " ";

    float gapok = 0.0;
    float gaber = 0.0;
    float tunja = 0.0;
    float pot = 0.0;

    jdl_aplikasi();
    input(nama, gol, status);
    hitungTunjanganGapok(gol, status, gapok, tunja);
    pot = hitungPotongan(gapok, tunja);
    gaber = hitungGajiBersih(gapok, tunja, pot);
    output(gapok, tunja, pot, gaber);

    return 0;
}
